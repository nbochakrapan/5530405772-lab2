package borchakrapan.naritsara.lap2;
import java.util.*;

public class SortNumbers {

	/**Naritsara Borchakrapan
	 * SudentID 553040577-2
	 * sec 2
	 */
	public static void main(String[] args) {
		String[] a = { args[0], args[1], args[2], args[3] };
		Arrays.sort(a);

		System.out.println("For the input numbers: ");
		System.out.print(args[0]);
		System.out.print(" " + args[1]);
		System.out.print(" " + args[2]);
		System.out.println(" " + args[3]);
		System.out.println("Sorted number: ");
		System.out.println(Arrays.toString(a));
	}

}


